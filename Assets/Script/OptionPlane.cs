using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionPlane : MonoBehaviour
{
    public GameObject optionPlane;
    public GameObject Bag;
    bool openningPlane=false;
    bool openningBag = false;
    public GameObject bagContent;
    public GameObject cell;
    public GameObject bagButton;
    public GameObject bagButtonWhiteBG;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DataScript.currentCharacter == null)
        {
            bagButton.SetActive(false);
            bagButtonWhiteBG.SetActive(false);
        }
        else
        {
            bagButton.SetActive(true);
            bagButtonWhiteBG.SetActive(true);
            while (bagContent.transform.childCount < DataScript.backpack.Count)
                AddCell();
            while (bagContent.transform.childCount > DataScript.backpack.Count)
                delectCell();

            if (bagContent.transform.childCount <= 8) bagContent.GetComponent<RectTransform>().sizeDelta = new Vector2(1227.1f, 690.54f);
            else bagContent.GetComponent<RectTransform>().sizeDelta = new Vector2(1227.1f, (320) * Mathf.Ceil(bagContent.transform.childCount / 4f));
        }

        if (Input.GetKeyDown(KeyCode.E) && DataScript.currentCharacter != null)
        {
            openBag();
        }

    }
    public void openOptionPlane()
    {
        if (!openningPlane) Camera.main.eventMask = 1 << 5;
        else Camera.main.eventMask = ~0;
        optionPlane.SetActive(!openningPlane);
        openningPlane = !openningPlane;
    }
    public void openBag()
    {
        if (DataScript.currentCharacter != null)
        {
            if (!openningPlane) Camera.main.eventMask = 1 << 5;
            else Camera.main.eventMask = ~0;
            Bag.SetActive(!openningBag);
            openningBag = !openningBag;
        }
       
    }
    void AddCell()
    {
        GameObject x = Instantiate(cell, bagContent.transform);
        x.name = (bagContent.transform.childCount - 1).ToString();
        x.transform.SetAsLastSibling();
    }
    void delectCell()
    {
        Destroy(bagContent.transform.GetChild(bagContent.transform.childCount - 1).gameObject);
    }

}
