using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingObjects : MonoBehaviour
{
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>(); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        rb.velocity = new Vector2(0, rb.velocity.y); 
    }
}
