using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;
using Spine.Unity;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoryControl : MonoBehaviour
{
    public GameObject camera;
    public GameObject dialogue;
    public GameObject textArea;
    public GameObject sayingArea;
    public GameObject girlPrefab, boyPrefab,monsterPrefab;
    public TextAsset StoryFlow;
    public GameObject ChoiceArea;
    public GameObject chooseButton;
    public GameObject Anouncement;
    public GameObject AllDark, GameOver;
    public GameObject CG;
    
    Dictionary<int, List<Coroutine>> routineDic = new Dictionary<int, List<Coroutine>>();

    List<List<string>> CommandLines = new List<List<string>>(); //儲存劇本指令
    public int pointer;
    public bool hasChosenChoice = false;


    // Start is called before the first frame update
    private void Awake()
    {
        pointer = 1;
        GetTextData(StoryFlow);
    }

    private void Start()
    {
        StartCoroutine(mainReadlines(0.05f));
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void GetTextData(TextAsset file)
    {
        CommandLines.Clear();
        CommandLines.Add(new List<string>()); //劇本從第一行開始，跳過第0行
        string[] lines = file.text.Split('\n');
        foreach (string line in lines)
        {
            if (!line.Equals(""))
            {
                List<string> tempList = new List<string>();
                string[] tempStrArr = line.Split(',');
                foreach (string str in tempStrArr)
                {
                    if (!str.Equals("") && str != null) tempList.Add(str);
                }
                CommandLines.Add(new List<string>(tempList));
            }
            else CommandLines.Add(new List<string>());
        }
    }

    public IEnumerator readStoryLine(int thisPointer , bool isMainThread)
    {
        List<string> line = CommandLines[thisPointer];
        GameObject controlObject=null;
        GameObject Prefab = null;

        print(string.Join(", ", line));
        switch (line[0])
        {
            case "ShiftCamera":
                int ShiftTime = (int)(float.Parse(line[3])*100);
                float xTarget = float.Parse(line[1]);
                float yTarget = float.Parse(line[2]);
                float xShift = (xTarget - camera.transform.position.x) / (float)ShiftTime;
                float yShift = (yTarget - camera.transform.position.y) / (float)ShiftTime;

                while (ShiftTime > 0)
                {
                    camera.transform.position = new Vector3(camera.transform.position.x + xShift,camera.transform.position.y + yShift, -10);
                    ShiftTime -= 1;
                    yield return new WaitForSeconds(0.01f);
                }
                yield break;

            case "LockCamera":
                camera.transform.position = new Vector3(float.Parse(line[1]), float.Parse(line[2]), -10);
                yield break;

            case "SetMainCharacter":
                camera.GetComponent<cameraScript>().followMode = false;
                camera.GetComponent<cameraScript>().followObj = null;
                if (line[1] == "Null")
                {
                    DataScript.currentCharacter = null;
                }
                else
                {
                    DataScript.currentCharacter = GameObject.Find(line[1]);
                    DataScript.currentCharacter.layer = 3;
                    DataScript.currentCharacter.GetComponent<MeshRenderer>().sortingOrder = 21;
                }
                yield break;

            case "Control":
                controlObject = GameObject.Find(line[1]);
                DataScript.ObjectsBeingControl.Add(controlObject);
                switch (line[2])
                {
                    case "HorizontalMoveTo":
                        yield return StartCoroutine(controlObject.GetComponent<ControlBySystem>().HorizontalMoveTo(float.Parse(line[3])));
                        break;
                    case "TurnAround":
                        yield return StartCoroutine(controlObject.GetComponent<ControlBySystem>().TurnAround(line[3]));
                        break;
                    case "MeetMonster": //only for marigol
                        yield return StartCoroutine(controlObject.GetComponent<ControlBySystem>().meetMonster());
                        break;
                    case "MonsterChase": //only for marigol
                        DataScript.ObjectsBeingControl.Remove(controlObject);
                        StartCoroutine(controlObject.GetComponent<ControlBySystem>().monsterChase());
                        break;
                    case "Surprice":
                        controlObject.GetComponent<Animator>().SetInteger("state", 1);
                        yield return new WaitForSeconds(0.1f);
                        controlObject.GetComponent<Animator>().SetInteger("state", 0);
                        break;
                    case "SayBubble":
                        GameObject sayArea = DataScript.findObjectFromParent("sayBubble", controlObject.transform);
                        sayArea.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = line[3];
                        sayArea.SetActive(true);
                        yield return new WaitForSeconds(float.Parse(line[4]));
                        sayArea.SetActive(false);
                        break;
                    case "Follow":
                        controlObject.GetComponent<ControlBySystem>().followObject = GameObject.Find(line[3]);
                        controlObject.GetComponent<ControlBySystem>().followMode = true;
                        break;
                    case "DisFollow":
                        controlObject.GetComponent<ControlBySystem>().followMode = false;
                        controlObject.GetComponent<ControlBySystem>().followObject = null;
                        break;
                    case "MonsterChaseFromStairs":  //only for monster
                        StartCoroutine(controlObject.GetComponent<ControlBySystem>().monsterChaseFromStairs());
                        break;
                    case "LookBack": //only for Marigol
                        yield return StartCoroutine(controlObject.GetComponent<ControlBySystem>().lookBack());
                        break;
                    case "Destroy":
                        Destroy(controlObject);
                        break;
                    case "Stop":
                        controlObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                        controlObject.GetComponent<Animator>().speed = 0;
                        break;
                    case "Continue":
                        controlObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                        controlObject.GetComponent<Animator>().speed = 1;
                        break;
                }
                if (DataScript.ObjectsBeingControl.Contains(controlObject)) DataScript.ObjectsBeingControl.Remove(controlObject);
                yield break;

            case "Instantiate":
                switch (line[1])
                {
                    case "Monster":
                        Prefab = monsterPrefab;
                        break;

                    case "Marigol":
                        Prefab = girlPrefab;
                        break;
                    case "Valerian":
                        Prefab = boyPrefab;
                        break;
                }
                float x = float.Parse(line[3]);
                float y = float.Parse(line[4]);
                GameObject insObject = Instantiate(Prefab, new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                insObject.name = line[2];
                yield break;

            case "DoInParallel":
                int start = Int32.Parse(line[1]);
                int end = Int32.Parse(line[2]);
                if (isMainThread) pointer += (end - start + 1);
                List<Coroutine> tempRouteList = new List<Coroutine>();
                for (int i = start; i <= end; i++)
                {
                    tempRouteList.Add(StartCoroutine(readStoryLine(i, false)));
                }
                foreach (Coroutine cr in tempRouteList)
                {
                    yield return cr;
                }
                yield break;

            case "Dialogue":
                start = Int32.Parse(line[1]);
                end = Int32.Parse(line[2]);
                GameObject originChar = DataScript.currentCharacter;
                DataScript.currentCharacter = null;
                if (isMainThread) pointer += (end - start + 1);
                yield return showDialogue(start, end);
                DataScript.currentCharacter = originChar;
                yield break;

            case "Choices":
                start = Int32.Parse(line[1]);
                end = Int32.Parse(line[2]);
                dialogue.SetActive(true);
                ChoiceArea.SetActive(true);
                for (int i = 0; i < ChoiceArea.transform.childCount; i++) Destroy(ChoiceArea.transform.GetChild(i).gameObject);
                for (int i = start; i <= end; i++)
                {
                    GameObject chooseBox = Instantiate(chooseButton,ChoiceArea.transform);
                    chooseBox.GetComponentInChildren<Text>().text = CommandLines[i][0];
                    chooseBox.GetComponent<choiceButtonScript>().branch = CommandLines[i][1];
                }
                yield return new WaitUntil(()=>(hasChosenChoice));
                hasChosenChoice = false;
                if (isMainThread) pointer += (end - start + 1);
                dialogue.SetActive(false);
                ChoiceArea.SetActive(false);
                yield break;

            case "Wait":
                yield return new WaitForSecondsRealtime(float.Parse(line[1]));
                yield break;

            case "Repeat":
                start = Int32.Parse(line[1]);
                end = Int32.Parse(line[2]);
                if (isMainThread) pointer += (end - start + 1);
                int repeatTime = Int32.Parse(line[3]);
                if (repeatTime < 9999)
                {
                    while (repeatTime > 0)
                    {
                        for (int i = start; i <= end; i++)
                        {
                            yield return StartCoroutine(readStoryLine(i, false));
                            yield return new WaitForSecondsRealtime(0.2f);
                        }
                        repeatTime--;
                    }
                }
                else
                {
                    while (true)
                        for (int i = start; i <= end; i++)
                        {
                            yield return StartCoroutine(readStoryLine(i, false));
                            yield return new WaitForSecondsRealtime(0.2f);
                        }
                }
                yield break;

            case "StopBackProgram":
                foreach (Coroutine rout in routineDic[Int32.Parse(line[1])])
                    StopCoroutine(rout);
                yield break;

            case "DoOnTheBack":
                start = Int32.Parse(line[1]);
                end = Int32.Parse(line[2]);
                routineDic.Add(pointer, new List<Coroutine>());
                routineDic[pointer].Add(StartCoroutine(DoOnTheBack(start, end, pointer)));
                if(isMainThread) pointer += (end - start + 1);
                yield break;

            case "AddBranches":
                for (int i = 1; i < line.Count; i++)
                {
                    string[] contents = line[i].Split(' ');
                    if (!(contents.Length<2)) DataScript.stroyBranches.Add(contents[0], Int32.Parse(contents[1]));
                    else break;
                }
                yield break;

            case "WaitBranch":
                yield return new WaitUntil(() => (DataScript.nextStoryBranch != null));
                print(DataScript.nextStoryBranch);
                pointer = DataScript.stroyBranches[DataScript.nextStoryBranch] - 1;
                DataScript.nextStoryBranch = null;
                yield break;

            case "Anouncement":
                Anouncement.GetComponent<Text>().text = line[1];
                Anouncement.SetActive(true);
                yield return new WaitForSecondsRealtime(float.Parse(line[2]));
                Anouncement.SetActive(false);
                yield break;

            //case "Restart":
                //need write
                //SceneManager.LoadScene(1);
                //pointer = 1;
                //yield break;

            case "GameOver":
                AllDark.SetActive(true);
                GameOver.SetActive(true);
                AllDark.GetComponent<Animator>().SetInteger("state", 1);
                GameOver.GetComponent<Animator>().SetInteger("state", 1);
                yield return new WaitForSeconds(GameOver.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length/ GameOver.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed);
                Anouncement.SetActive(true);
                Anouncement.GetComponent<Text>().text = line[1];
                yield break;

            case "ShowCG":
                Sprite CGsprite = Resources.Load<Sprite>(line[1]);
                CG.GetComponent<Image>().sprite = CGsprite;
                CG.SetActive(true);
                yield break;

            case "TurnOffCG":
                CG.SetActive(false);
                yield break;

            case "Get":
                DataScript.backpack.Add(gameData.GetObjectIdByName(line[1]));
                yield break;

            case "ShowItemDescribe":
                missionObjDescrip.id = gameData.GetObjectIdByName(line[1]);
                missionObjDescrip.show();
                yield break;

            case "ShakeCamera":
                camera.GetComponent<cameraScript>().shakeCamera = true;
                yield return new WaitForSeconds(camera.GetComponent<cameraScript>().shakeTime);
                yield break;

            case "WaitAnyKey":
                DataScript.waitForAnyKey = true;
                yield return new WaitUntil(()=>(!DataScript.waitForAnyKey));
                yield break;

            case "CameraFollow":
                camera.GetComponent<cameraScript>().followMode = true;
                camera.GetComponent<cameraScript>().followObj = GameObject.Find(line[1]);
                yield break;

            case "CameraDisfollow":
                camera.GetComponent<cameraScript>().followMode = false;
                camera.GetComponent<cameraScript>().followObj = null;
                yield break;
        }
    }


    IEnumerator showDialogue(int start, int end)
    {
        dialogue.transform.GetChild(0).gameObject.SetActive(false);
        dialogue.transform.GetChild(1).gameObject.SetActive(false);
        dialogue.transform.GetChild(2).gameObject.SetActive(false);

        textArea.GetComponent<textUIScript>().textList.Clear();
        textArea.GetComponent<textUIScript>().pointer = 0;
        var textList = textArea.GetComponent<textUIScript>().textList;
        for (int i = start; i <= end; i++) {
            textList.Add(new List<string>(CommandLines[i]));
        }
        textArea.SetActive(true);
        dialogue.SetActive(true);
        StartCoroutine(textArea.GetComponent<textUIScript>().showDialogue());

        yield return new WaitUntil(()=>(DataScript.textDialogueFinished));
        DataScript.textDialogueFinished = false;
        textArea.SetActive(false);
        dialogue.SetActive(false);

    }

    IEnumerator mainReadlines(float runWaitTime)
    {
        while (pointer<=CommandLines.Count)
        {
            yield return readStoryLine(pointer,true);
            pointer++;
            yield return new WaitForSecondsRealtime(runWaitTime);

        }
    }

    IEnumerator DoOnTheBack(int start, int end, int ownLineNumber)
    {
        yield return null;
        for (int i = start; i <= end; i++)
        {
            Coroutine route = StartCoroutine(readStoryLine(i, false));
            routineDic[ownLineNumber].Add(route);
            yield return route;
        }
   
    }

   

}
