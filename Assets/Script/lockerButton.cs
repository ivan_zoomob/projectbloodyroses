using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class lockerButton : MonoBehaviour
{
    public GameObject clone;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void makeOutline()
    {
        if (clone == null)
        {
            GameObject outlinePic = Instantiate(this.gameObject);
            Destroy(outlinePic.GetComponent<EventTrigger>());
            Destroy(outlinePic.GetComponent<Button>());
            Image outlinePicImage = outlinePic.GetComponent<Image>();
            outlinePicImage.color = new Color(195, 63, 80, 150);
            outlinePicImage.maskable = false;
            outlinePicImage.raycastTarget = false;
            outlinePic.transform.position = transform.position;
            outlinePicImage.material = transform.parent.parent.gameObject.GetComponent<InteractObject>().outlineMaterial;
            outlinePic.transform.SetParent(transform.parent);
            outlinePic.transform.SetSiblingIndex(transform.GetSiblingIndex());
            outlinePic.GetComponent<RectTransform>().localScale = new Vector3(1.1f, 1.1f, 1);


            clone = outlinePic;
        }
        else clone.SetActive(true);


    }
    public void PointerExit()
    {
        clone.SetActive(false);
    }
}
