using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class missionObjects : MonoBehaviour
{
    public int id=0;
    public string itsName;
    public string description;
    public Sprite sprite;
    public bool hasBeenLightened=false;
    bool touching = false;
    public GameObject keyLogo;
    public string keyLogoOriginKey;

    SpriteRenderer objectLogo;
    // Start is called before the first frame update
    public void updateData(int i)
    {
        this.id = gameData.missionObjs[i].id;
        this.itsName = gameData.missionObjs[i].name;
        this.description = gameData.missionObjs[i].description;
        this.sprite = gameData.missionObjs[i].sprite;

        objectLogo.sprite = sprite;
    }

    private void Awake()
    {
        objectLogo = GetComponent<SpriteRenderer>();
        keyLogo = DataScript.findFromAllObject("keyLogo"); //F key logo
    }

    void Start()
    {
        updateData(id);
        objectLogo.color = new Color32(255, 255, 255, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetKey(KeyCode.Space))
            transform.GetChild(1).gameObject.SetActive(false);

        if (DataScript.currentCharacter == null)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
        }


        if (hasBeenLightened && touching && Input.GetKey(KeyCode.F))
        {
            if (id == 8)
            {
                DataScript.nextStoryBranch = "key";
            }
            else
            {
                DataScript.backpack.Add(id);
                if (DataScript.isFirstTimeGet[id])
                {
                    missionObjDescrip.id = id;
                    missionObjDescrip.show();
                }

                Destroy(this.gameObject);
            }
        }
        
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter && hasBeenLightened && DataScript.characterInteractable)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            if (keyLogo.GetComponent<keyLogo>().keyCode!="F") keyLogoOriginKey = keyLogo.GetComponent<keyLogo>().keyCode;
            keyLogo.GetComponent<keyLogo>().keyCode = "F";
            keyLogo.SetActive(true);
            touching = true;
        }
        if (collision.gameObject.name == "flashLightR" || collision.gameObject.name == "flashLightL" &&!touching)
        {
            transform.GetChild(1).gameObject.SetActive(true);
            if (!hasBeenLightened) hasBeenLightened = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter && hasBeenLightened && DataScript.characterInteractable)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            keyLogo.GetComponent<keyLogo>().keyCode = keyLogoOriginKey;
            if (!keyLogo.GetComponent<keyLogo>().keepOpening) keyLogo.SetActive(false);
            touching = false;
        }
        if (collision.gameObject.name == "flashLightR" || collision.gameObject.name == "flashLightL" && !touching)
        {
            transform.GetChild(1).gameObject.SetActive(false);
        }
    }
}
