using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationManager : MonoBehaviour
{
    [Header("Conversation component")]
    [SerializeField] GameObject conversationPanel;
    [SerializeField] Image characterLeft;
    [SerializeField] Image characterRight;
    [SerializeField] TMPro.TextMeshProUGUI conversationName;
    [SerializeField] TMPro.TextMeshProUGUI conversationContent;

    public void EnableConversationPanel(bool stat)
    {
        conversationPanel.SetActive(stat);
    }
}
