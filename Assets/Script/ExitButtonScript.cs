using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExitButtonScript : MonoBehaviour
{
    public void ExitThisPage()
    {
        var parentObject = transform.parent;
        parentObject.GetComponent<CanvasGroup>().alpha = 0;
        parentObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
        parentObject.GetComponent<CanvasGroup>().interactable = false;   
    }
    public void DisactiveThisPage()
    {
        GameObject par = transform.parent.gameObject;
        par.SetActive(false);
    }
    public void infoPageClose()
    {
        int id = missionObjDescrip.id;
        if (DataScript.isFirstTimeGet[id])
        {
            missionObjDescrip.firstTimeClose();
            DataScript.isFirstTimeGet[id] = false;
        }
        else missionObjDescrip.close();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
