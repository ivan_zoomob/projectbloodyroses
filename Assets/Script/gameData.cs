using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameData : MonoBehaviour
{
    public static List<missionObj> missionObjs = new List<missionObj>();
    public Sprite secreteBookmark;
    public Sprite flashLight;
    public Sprite photo;
    public Sprite religionBook;
    public Sprite key;
    public Sprite clamp;
    public Sprite diary1,diary2;
    public Sprite menu;

    public class missionObj
    {
        public int id;
        public string name;
        public string description;
        public Sprite sprite;

        public missionObj(int id,string name,string description, Sprite sprite)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.sprite = sprite;
        }

    }
    

    private void Awake()
    {
        missionObjs.Add(new missionObj(0, "詭異書籤", "充滿神秘的書籤。\n\n用途不明。", secreteBookmark));
        missionObjs.Add(new missionObj(1, "手電筒", "電量不多的電筒。\n\n（按空格鍵使用）", flashLight));
        missionObjs.Add(new missionObj(2, "某二人的合照", "不知道什麼時候的合照。\n\n女的好像看著有點眼熟。。。", photo));
        missionObjs.Add(new missionObj(3, "散發邪惡的教典", "看起來很危險的書。\n\n裡面的內容好像是關於某個神秘宗教的教義。", religionBook));
        missionObjs.Add(new missionObj(4, "夾鉗", "一把普通的夾鉗。\n\n或者可以用來維修什麼。", clamp));
        missionObjs.Add(new missionObj(5, "奇怪的食譜", "一張單薄的食譜。\n\n裡面的菜式好像有點怪異。", menu));
        missionObjs.Add(new missionObj(6, "日記的第一頁", "不知道是誰的日記。\n\n好像是第一頁的樣子", diary1));
        missionObjs.Add(new missionObj(7, "日記的第二頁", "不知道是誰的日記。\n\n好像是第二頁的樣子", diary2));
        missionObjs.Add(new missionObj(8, "防火門的鑰匙", "一把平平無奇的鑰匙。\n\n從款式來推斷，大概是類似防火門的鑰匙吧。", key));

    }

    public static int GetObjectIdByName(string name)
    {
        foreach(missionObj x in missionObjs)
        {
            if (x.name == name) return x.id;
        }
        return 999;
    }
}
