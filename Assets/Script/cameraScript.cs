using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScript : MonoBehaviour
{
    public GameObject CG;
    public GameObject BlackBG;

    public bool shakeCamera=false;
    public float shakeTime;
    public float tempShakeTime;
    Vector3 originPos;
    public float shakeRange;
    public bool followMode=false;
    public GameObject followObj=null;

    GameObject shadow;


    void Start()
    {
        shadow = transform.GetChild(0).gameObject;
        shakeTime = 1;
        shakeRange = 0.5f;
        tempShakeTime = shakeTime;
        
    }

    void Update()
    {
        shadow.transform.position = new Vector3(shadow.transform.position.x, shadow.transform.position.y, 0);

        if (shakeCamera)
        {
            if (CG.active)
            {
                BlackBG.SetActive(true);
                CG.transform.localPosition = new Vector3((Random.value * 2 - 1)* Mathf.Pow(shakeTime+1, 2) * 6   , (Random.value * 2 - 1)* Mathf.Pow(shakeTime+1, 2) * 6, 0);
            }
            else
            {
                transform.position = new Vector3((Random.value * 2 - 1) * Mathf.Pow(shakeTime, 2) * shakeRange + originPos.x, (Random.value * 2 - 1) * Mathf.Pow(shakeTime, 2) * shakeRange + originPos.y, -10);
            }
                

            tempShakeTime -= Time.deltaTime;
            if (tempShakeTime <= 0)
            {
                shakeCamera = false;
                tempShakeTime = shakeTime;
                if (CG.active)
                {
                    CG.transform.localPosition = Vector3.zero;
                    BlackBG.SetActive(false);
                }
            }
        }
        else {
            originPos = transform.position;
            if (followMode)
                transform.position = new Vector3(followObj.transform.position.x, followObj.transform.position.y + 5, -10);
            else if (DataScript.currentCharacter != null)
                transform.position = new Vector3 (DataScript.currentCharacter.transform.position.x, DataScript.currentCharacter.transform.position.y+5, -10); //攝影機跟隨角色
        }
        
    }

    public IEnumerator zoomCamera(float time)
    {
        float originTime = time;
        while (time >=0)
        {
            GetComponent<Camera>().orthographicSize -= (0.06f)*(time/originTime);
            time -= 0.05f;
            yield return new WaitForSeconds(0.05f);
        }
    }

    
}
