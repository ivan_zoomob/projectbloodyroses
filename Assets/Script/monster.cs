using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class monster : MonoBehaviour
{
    Rigidbody2D rb;
    BoxCollider2D box;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator chasing()
    {
        GetComponent<Animator>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;
        GameObject marigol = DataScript.currentCharacter;

        if (marigol.transform.position.x >= transform.position.x)
        {
            GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "runR", true);
            rb.velocity = new Vector2(3, 0);
        }
        else if (marigol.transform.position.x < transform.position.x)
        {
            GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "runL", true);
            GetComponent<Animator>().SetInteger("state", 2);

            yield return new WaitUntil(() => (transform.position.x<-17 && transform.position.y<=-5.42));
            //need add animation here

            yield return new WaitUntil(()=>(transform.position.y<=-9.4));
            GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "runR", true);
            GetComponent<Canvas>().sortingOrder = 22;

        }
        yield break;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter)
        {
            rb.velocity = Vector2.zero;
            DataScript.currentCharacter.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            DataScript.currentCharacter = null;
            StartCoroutine(Camera.main.GetComponent<cameraScript>().zoomCamera(2));
            DataScript.nextStoryBranch = "catchedByMonster";
        }
        
    }


}
