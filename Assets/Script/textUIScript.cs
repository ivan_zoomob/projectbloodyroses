using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class textUIScript : MonoBehaviour
{
    public List<List<string>> textList = new List<List<string>>();
    public int pointer = 0;
    float textSpeed=0.06f;
    public Text textBox,nameBox;
    bool textFinish;
    Dictionary<string, string> picPosition=new Dictionary<string, string>();

    public Sprite girlNormal;
    public Sprite girlScared;
    public Sprite girlNotHappy;
    public Sprite girlQues;
    public Sprite girlAngry;
    public Sprite boyNormal;
    public Sprite boyEhh;
    public Sprite boyQues;
    public Sprite boyAngry;

    public GameObject PicL;
    public GameObject PicM;
    public GameObject PicR;
    public Dictionary<string, Sprite> boyDic = new Dictionary<string, Sprite>();
    public Dictionary<string, Sprite> girlDic= new Dictionary<string, Sprite>();
    public string lastSpeaker=null;

    bool shakingCamera = false;


    void Start()
    {
        boyDic.Add("normal", boyNormal);
        boyDic.Add("ehh", boyEhh);
        boyDic.Add("ques", boyQues);
        boyDic.Add("angry", boyAngry);

        girlDic.Add("normal",girlNormal);
        //girlDic.Add("ehh",);
        girlDic.Add("ques",girlQues);
        girlDic.Add("angry",girlAngry);
        girlDic.Add("notHappy",girlNotHappy);

        picPosition.Add("Marigol", "null");
        picPosition.Add("Valerian", "null");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) {
            if (pointer == textList.Count)
            {
                DataScript.textDialogueFinished = true;
                this.gameObject.SetActive(false);
            }  
            else if (textFinish ) StartCoroutine(showDialogue());
            else if (!textFinish && !shakingCamera) textFinish = true;
        }
    }

    public IEnumerator showDialogue()
    {

        if (textList[pointer][0] == "SetPic")
        {
            print(textList[pointer][1]+" "+textList[pointer][2]+" "+textList[pointer][3]);
            Dictionary<string, Sprite> tempDict=new Dictionary<string, Sprite>();
            if (textList[pointer][1] == "Marigol") tempDict = girlDic;
                else if (textList[pointer][1] == "Valerian") tempDict = boyDic;
            
            if (textList[pointer][2] == "null")
            {
                picPosition[textList[pointer][1]] = "null";
                switch (textList[pointer][3])
                {
                    case "L":
                        PicL.SetActive(false);
                        break;
                    case "R":
                        PicR.SetActive(false);
                        break;
                    case "M":
                        PicM.SetActive(false);
                        break;
                }
            }
            else
            {
                Sprite tempSprite = tempDict[textList[pointer][2]];
                switch (picPosition[textList[pointer][1]].ToCharArray()[0])
                {
                    case 'L':
                        PicL.SetActive(false);
                        break;
                    case 'R':
                        PicR.SetActive(false);
                        break;
                    case 'M':
                        PicM.SetActive(false);
                        break;
                }
                
                picPosition[textList[pointer][1]] = textList[pointer][3];
                switch (textList[pointer][3].ToCharArray()[0])
                {
                    case 'L':
                        PicL.GetComponent<Image>().sprite = tempSprite;
                        PicL.SetActive(true);
                        break;
                    case 'R':
                        PicR.GetComponent<Image>().sprite = tempSprite;
                        PicR.SetActive(true);
                        break;
                    case 'M':
                        PicM.GetComponent<Image>().sprite = tempSprite;
                        PicM.SetActive(true);
                        break;  
                }
            }
            pointer++;
            yield return showDialogue();
        }
        else
        {  
            if (lastSpeaker != null && lastSpeaker!="")
            {
                switch (picPosition[lastSpeaker].ToCharArray()[0])
                {
                    case 'L':
                        PicL.GetComponent<Image>().color = new Color32(100, 100, 100, 255);
                        break;
                    case 'R':
                        PicR.GetComponent<Image>().color = new Color32(100, 100, 100, 255);
                        break;
                    case 'M':
                        PicM.GetComponent<Image>().color = new Color32(100, 100, 100, 255);
                        break;
                }
            }
            
            if (!(textList[pointer][0] == "System"))
            {
                switch ( picPosition[ textList[pointer][0] ].ToCharArray()[0] )
                {
                    case 'L':
                        PicL.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                        break;
                    case 'R':
                        PicR.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                        break;
                    case 'M':
                        PicM.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                        break;
                }
                lastSpeaker = textList[pointer][0];
            }
            textFinish = false;
            nameBox.text = textList[pointer][0];
            
            if (textList[pointer][1] == "ShakeCamera")
            {
                Camera.main.GetComponent<cameraScript>().shakeCamera = true;
                textBox.text = textList[pointer][2];
                shakingCamera = true;
                yield return new WaitForSeconds(Camera.main.GetComponent<cameraScript>().shakeTime);
                shakingCamera = false;
                pointer++;
                textFinish = true;
            }
            else
            {
                char[] saying;
                saying = textList[pointer][1].ToCharArray();
                textBox.text = null;
                for (int i = 0; i < saying.Length; i++)
                {
                    if (textFinish) break;
                    textBox.text += saying[i].ToString();
                    yield return new WaitForSeconds(textSpeed);
                }
                textBox.text = textList[pointer][1];
                pointer++;
                textFinish = true;
            }
            
        }
        
    }


}

