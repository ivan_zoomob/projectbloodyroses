using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enterWithoutLight : MonoBehaviour
{
    int count = 0;
    public GameObject dialogue;
    public GameObject textArea;
    public GameObject missionObject;
    GameObject originChar = null;

    GameObject secretBookMark;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DataScript.lightElectric!=null) this.gameObject.GetComponent<EdgeCollider2D>().isTrigger = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter)
        {
            StartCoroutine(noLight());
            if (count == 3)
            {
                secretBookMark = Instantiate(missionObject);
                secretBookMark.transform.position = originChar.transform.position;
                secretBookMark.GetComponent<SpriteRenderer>().sortingOrder = originChar.GetComponent<MeshRenderer>().sortingOrder;
                secretBookMark.GetComponent<missionObjects>().updateData(0);

            }
        }
    }

    IEnumerator noLight()
    {
        originChar = DataScript.currentCharacter;
        DataScript.currentCharacter = null;
        count++;
        textArea.GetComponent<textUIScript>().textList.Clear();
        textArea.GetComponent<textUIScript>().pointer = 0;
        string[] str = { "Marigol", "前面好像有點黑...我應該先找個手電筒嗎？" };
        textArea.GetComponent<textUIScript>().textList.Add(new List<string>(str));
        textArea.SetActive(true);

        dialogue.transform.GetChild(0).gameObject.SetActive(false);
        dialogue.transform.GetChild(1).gameObject.SetActive(false);
        dialogue.transform.GetChild(2).gameObject.SetActive(false);
        dialogue.SetActive(true);
        StartCoroutine(textArea.GetComponent<textUIScript>().showDialogue());

        yield return new WaitUntil(() => (DataScript.textDialogueFinished));
        DataScript.textDialogueFinished = false;
        textArea.SetActive(false);
        dialogue.SetActive(false);
        DataScript.currentCharacter = originChar;
    }
}
