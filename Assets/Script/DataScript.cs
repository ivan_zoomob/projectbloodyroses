using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataScript : MonoBehaviour
{
    public static bool characterInteractable = true;
    public static string characterDirection; //當前控制角色的方向
    public static GameObject currentCharacter ; //當前控制角色
    public static bool horizontalControlEnable = true; //x軸可控
    public static bool verticalControlEnable = false; //y軸可控
    public static float? lightElectric = null; //電筒電力
    public static bool canCollide = true; //用來判斷角色可否碰撞物體（用作推動物體）
    public static string nextStoryBranch = null;
    public static bool textDialogueFinished = false;
    public static Dictionary<string, int> stroyBranches = new Dictionary<string, int>();
    public static float strength = 1;
    public static bool isOnFloor=true;
    public static List<GameObject> ObjectsBeingControl = new List<GameObject>();
    public static List<int> backpack = new List<int>();
    public static Dictionary<int, bool> isFirstTimeGet = new Dictionary<int, bool>();
    public static bool waitForAnyKey = false;
    public static bool playedMiniGame=false;
    public GameObject keyLogo;
    public GameObject lightBarUI;
    public Image energyBarFillAmount;
    public CanvasGroup energyBarCanvasGp;

    
    public static int dataNumber; //用來儲存遊玩紀錄, not yet done

    void Start()
    {
        foreach (gameData.missionObj x in gameData.missionObjs)
        {
            isFirstTimeGet.Add(x.id, true);
        }
        energyBarFillAmount = GameObject.Find("energyBarFillAmount").GetComponent<Image>();
        energyBarCanvasGp = GameObject.Find("EnergyBar").GetComponent<CanvasGroup>();

    }

    void Update()
    {
        if (waitForAnyKey)
        {
            if (Input.anyKey)
            {
                waitForAnyKey = false;
            }
        }

        if (currentCharacter != null)
        {
            if (!(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.LeftCommand)))
            {
                energyBarCanvasGp.alpha = 0.15f;
                if (strength < 1)
                    strength += (Time.deltaTime * 0.2f);
            }

            else
            {
                energyBarCanvasGp.alpha = 1;
                if (strength>0) strength -= (Time.deltaTime * 0.1f);
            }
            energyBarFillAmount.fillAmount = strength;
        }
        else
        {
            energyBarCanvasGp.alpha = 0;
            lightBarUI.SetActive(false);
        }

    }
    public static GameObject findFromAllObject(string itsName)
    {
        foreach (GameObject x in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
        {
            if (x.name == itsName) return x;
        }
        return null;
    }
    public static GameObject findObjectFromParent(string itsName,Transform parent)
    {
        int childNum = parent.childCount;
        for (int i = 0; i < childNum; i++)
        {
            if (parent.GetChild(i).name == itsName) return parent.GetChild(i).gameObject;
        }
        return null;
    }

}
