using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movePieceInMinigame : MonoBehaviour
{
    Vector3 originPos;
    bool haveOutline = false;
    GameObject clone;
    bool isInside = false;
    Vector3 placePos;
    public GameObject targetPlace;

    // Start is called before the first frame update
    void Start()
    {
        targetPlace.GetComponent<BoxCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDrag()
    {
        targetPlace.GetComponent<BoxCollider2D>().enabled = true;
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.gameObject.transform.position = new Vector3(mousePos.x,mousePos.y,0);
        if (clone!=null) clone.transform.position = transform.position;
    }
    private void OnMouseUp()
    {
        if (!isInside)
        {
            transform.position = originPos;
            clone.transform.position = originPos;
        }
        else
        {
            transform.position = placePos;
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(clone);
            transform.parent.parent.parent.gameObject.GetComponent<InteractObject>().count+=1;
        }
        targetPlace.GetComponent<BoxCollider2D>().enabled = false;

    }
    private void OnMouseDown()
    {
        if (!isInside)
            originPos = transform.position;
    }
    private void OnMouseEnter()
    {
        if (!isInside)
        {
            if (!haveOutline)
            {
                makeOutline();
                haveOutline = true;
            }
            else if (clone!=null) clone.SetActive(true);
        }
            

    }
    private void OnMouseExit()
    {
        if (!isInside)
            if (!(clone==null)) clone.SetActive(false);

    }

    public void makeOutline()
    {
        GameObject outlinePic = Instantiate(this.gameObject);
        Destroy(outlinePic.GetComponent<movePieceInMinigame>());
        Destroy(outlinePic.GetComponent<BoxCollider2D>());
        Image outlinePicImage = outlinePic.GetComponent<Image>();
        outlinePicImage.color = new Color(195, 63, 80, 150);
        outlinePicImage.maskable = false;
        outlinePicImage.raycastTarget = false;
        outlinePic.transform.position = transform.position;
        outlinePicImage.material = transform.parent.parent.parent.gameObject.GetComponent<InteractObject>().outlineMaterial;
        outlinePic.transform.SetParent(transform.parent);
        outlinePic.transform.SetSiblingIndex(1);
        outlinePic.GetComponent<RectTransform>().localScale = new Vector3(0.41f, 0.41f, 1);
       

        clone = outlinePic;


    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject==targetPlace)
        {
            Bounds bound = collision.GetComponent<BoxCollider2D>().bounds;
            bound.center = new Vector3(collision.GetComponent<BoxCollider2D>().bounds.center.x, collision.GetComponent<BoxCollider2D>().bounds.center.y, 0);
            if (bound.Contains(transform.position))
            {
                isInside = true;
                placePos = collision.transform.position;
            }

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == targetPlace)
            isInside = false;
    }
    public void GameOver()
    {
        if (clone != null) Destroy(clone);
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        GetComponent<Rigidbody2D>().gravityScale = 4.5f;
    }

}
