using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIbuttonScript : MonoBehaviour
{
    public bool pointer = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void pointerIn()
    {
        pointer = true;
    }

    public void pointerOut()
    {
        pointer = false;
    }

    public void continueClick()
    {
        GameObject.Find("ContinuePage").GetComponent<CanvasGroup>().alpha = 1;
        GameObject.Find("ContinuePage").GetComponent<CanvasGroup>().blocksRaycasts = true;
        GameObject.Find("ContinuePage").GetComponent<CanvasGroup>().interactable = true;
    }

    public void optionClick()
    {
        GameObject.Find("OptionPage").GetComponent<CanvasGroup>().alpha = 1;
        GameObject.Find("OptionPage").GetComponent<CanvasGroup>().blocksRaycasts = true;
        GameObject.Find("OptionPage").GetComponent<CanvasGroup>().interactable = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (pointer)
        {
            transform.GetChild(0).gameObject.GetComponent<Image>().enabled = true;
            
        }
        else
        {
            transform.GetChild(0).gameObject.GetComponent<Image>().enabled = false;
        }
    }
    public void startNewGame()
    {
        SceneManager.LoadScene(1);
    }
}
