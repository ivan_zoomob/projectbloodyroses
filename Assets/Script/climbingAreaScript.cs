using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class climbingAreaScript : MonoBehaviour
{

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DataScript.isOnFloor)
        {
            foreach (var x in GetComponentsInChildren<EdgeCollider2D>()) x.isTrigger = true;
            if (transform.parent.tag == "movingObject") transform.parent.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        }    
        else
        {
            foreach (var x in GetComponentsInChildren<EdgeCollider2D>()) x.isTrigger = false;
            if (transform.parent.tag == "movingObject") transform.parent.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
            


    }

    public bool checkCharacterInside() //判斷角色是否在物件範圍內
    {
        Vector3 pos = DataScript.currentCharacter.transform.position;
        Bounds bounds = this.GetComponent<BoxCollider2D>().bounds;
        return bounds.Contains(pos);
    }

    public void setUnableToCollide()
    {
        DataScript.canCollide = false; //爬樓梯時不可碰撞
    }
    
    
}
