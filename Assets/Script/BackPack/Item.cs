using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Item : MonoBehaviour
{
    [Header("For system use")]
    public int itemIndex;

    [Header("For display use")]
    public Sprite itemSprite;
    public String itemName;
    public String itemDescription;
}
