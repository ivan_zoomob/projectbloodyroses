using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public List<Item> items;

    BackPack backPack;
    private void Start()
    {
        backPack = FindObjectOfType<BackPack>();
    }

    public void AddItemToBackPack(int index)
    {
        for(int i=0;i<items.Count;i++)
        {
            if (items[i].itemIndex == index)
            {
                backPack.AddItemToBackPack(items[i]);
                break;
            }
        }
    }
}
