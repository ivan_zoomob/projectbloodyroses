using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackPackItemViewer : MonoBehaviour
{
    [Header("itemViewerComponents")]
    [SerializeField] CanvasGroup itemViewer;                //item viewer canvas
    [SerializeField] Image itemImage;                      
    [SerializeField] TMPro.TextMeshProUGUI itemNameText;
    [SerializeField] TMPro.TextMeshProUGUI itemDescText;

    public void ViewItem(Item itemToBeDisplay)              //update item image, name, desc & show popup
    {
        itemImage.sprite = itemToBeDisplay.itemSprite;
        itemNameText.text = itemToBeDisplay.itemName;
        itemDescText.text = itemToBeDisplay.itemDescription;
        EnableItemViewer(true);
    }

    public void ButtonClose()   //close popup
    {
        EnableItemViewer(false);
    }

    void EnableItemViewer(bool stat)
    {
        if (isItemViewerEnabling == false)
        {
            StartCoroutine(IEEnableItemViewer(stat));
        }
    }

    bool isItemViewerEnabling = false;  //bool to prevent doing multiple times
    float timer;    //fading timer
    IEnumerator IEEnableItemViewer(bool stat)
    {
        isItemViewerEnabling = true;
        timer = 0;
        int startAlpha;
        int endAlpha;
        if(stat == true)    //determine fade in or out
        {
            startAlpha = 0;
            endAlpha = 1; 
            itemViewer.gameObject.SetActive(true);
        }
        else
        {
            startAlpha = 1;
            endAlpha = 0;
        }

        itemViewer.alpha = startAlpha;
        while (timer < 0.25f)    //fade, duration of 0.5s
        {
            itemViewer.alpha = Mathf.Lerp(startAlpha, endAlpha, timer/0.25f);
            timer += Time.deltaTime;
            yield return null;
        }
        itemViewer.alpha = endAlpha;
        itemViewer.gameObject.SetActive(stat);

        isItemViewerEnabling = false;
    }
}
