using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackPackItemSlot : MonoBehaviour
{
    [Header("icon in backpack")]
    [SerializeField]Image itemSlotImage;

    Item heldItem;
    BackPackItemViewer itemViewer;

    public void SetItemData(Item item, BackPackItemViewer viewer)   //assign item to the slot
    {
        heldItem = item;
        itemSlotImage.sprite = heldItem.itemSprite;
        itemViewer = viewer;
    }

    public void ButtonShowItemDetails()     //show item details
    {
        itemViewer.ViewItem(heldItem);
    }
}
