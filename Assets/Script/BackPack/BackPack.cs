using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackPack : MonoBehaviour
{
    [SerializeField] CanvasGroup backPack;
    [SerializeField] BackPackItemViewer itemViewer;
    [SerializeField] GameObject backPackSlotPrefab;
    [SerializeField] GameObject backPackLocation;

    bool isBackPackOn = false;

    public void AddItemToBackPack(Item itemToBeAdded)   //spawn a slot in backpack
    {
        BackPackItemSlot backPackItemSlot = Instantiate(backPackSlotPrefab, backPackLocation.transform).GetComponent<BackPackItemSlot>();
        backPackItemSlot.SetItemData(itemToBeAdded, itemViewer);    //assign the item to the slot, itemViewr use for display item detail
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            EnableBackPack(!isBackPackOn);
        }
    }

    public void ButtonClose()   //close popup
    {
        EnableBackPack(false);
    }

    public void EnableBackPack(bool stat)
    {
        if(isBackPackEnabling == false)
        {
            StartCoroutine(IEEnableBackPack(stat));
            isBackPackOn = !isBackPackOn;
        }
    }

    bool isBackPackEnabling = false;  //bool to prevent doing multiple times
    float timer;    //fading timer
    IEnumerator IEEnableBackPack(bool stat)
    {
        isBackPackEnabling = true;
        timer = 0;
        int startAlpha;
        int endAlpha;
        if (stat == true)    //determine fade in or out
        {
            startAlpha = 0;
            endAlpha = 1;
            backPack.gameObject.SetActive(true);
        }
        else
        {
            startAlpha = 1;
            endAlpha = 0;
        }

        backPack.alpha = startAlpha;
        while (timer < 0.25f)    //fade, duration of 0.5s
        {
            backPack.alpha = Mathf.Lerp(startAlpha, endAlpha, timer / 0.25f);
            timer += Time.deltaTime;
            yield return null;
        }
        backPack.alpha = endAlpha;
        backPack.gameObject.SetActive(stat);

        isBackPackEnabling = false;
    }
}
