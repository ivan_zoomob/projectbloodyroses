using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class ControlBySystem : MonoBehaviour
{
    public Rigidbody2D rb;
    public float originSpeed;
    public float followSpeed;
    public SkeletonAnimation spineComponent;
    public string direction;
    public string animationName = null;


    public bool followMode = false;
    public GameObject followObject=null;
    // Start is called before the first frame update
    void Start()
    {
        spineComponent = this.GetComponent<SkeletonAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (followMode && followObject!=null)
        {
            if (Mathf.Abs(transform.position.x - followObject.transform.position.x) > 3.5)
            {
                rb.velocity = new Vector2((transform.position.x > followObject.transform.position.x ? -followSpeed : followSpeed), 0);
                direction = transform.position.x > followObject.transform.position.x ? "L" : "R";
                animation();
            }

            else
            {
                rb.velocity = Vector2.zero;
                animation();
            }
        }
    }

    public IEnumerator TurnAround(string inputDirection)
    {
        rb.velocity = Vector2.zero;
        direction = inputDirection;
        animation();
        yield break;
    }

    public IEnumerator HorizontalMoveTo(float x)
    {
        rb.bodyType = RigidbodyType2D.Kinematic;
        direction = (transform.position.x <= x ? "R" : "L");
        movement();
        animation();
        yield return new WaitUntil(()=>(direction=="R"? transform.position.x >= x : transform.position.x <= x));
        rb.velocity = new Vector2(0, rb.velocity.y);
        animation();
        rb.bodyType = RigidbodyType2D.Dynamic;

    }

    void movement() {
        switch (direction)
        {
            case "L":
                rb.velocity = new Vector2(-originSpeed, rb.velocity.y);
                break;
            case "R":
                rb.velocity = new Vector2(originSpeed, rb.velocity.y);
                break;
        }
    }
    void animation()
    {
        if (animationName!=null && animationName != "")
        {
            spineComponent.AnimationName = animationName;
        }
        else if (rb.velocity.x != 0)
        {
            switch (direction)
            {
                case "L":
                    spineComponent.AnimationName = "runL";
                    break;
                case "R":
                    spineComponent.AnimationName = "runR";
                    break;
            }
        }
        else 
        {
            switch (direction)
            {
                case "L":
                    spineComponent.AnimationName = "idleL";
                    break;
                case "R":
                    spineComponent.AnimationName = "idleR";
                    break;
                case "F":
                    spineComponent.AnimationName = "idle";
                    break;
                case "B":
                    spineComponent.AnimationName = "back";
                    break;
            }
        }
    }

    public IEnumerator meetMonster()
    {
        yield return new WaitUntil(() => (InteractObject.waitForMeetMonster));

        DataScript.currentCharacter = null; //control by system
        Camera cam = Camera.main;
        GameObject monster = DataScript.findFromAllObject("monsterForAni");

        yield return new WaitForSeconds(0.6f);

        cam.GetComponent<cameraScript>().tempShakeTime = 2;  //first time shake camera
        cam.GetComponent<cameraScript>().shakeCamera = true;

        yield return new WaitForSeconds(0.5f);

        spineComponent.AnimationName = "LPair";
        spineComponent.timeScale = 0;
        DataScript.findObjectFromParent("L", transform).SetActive(true);
        DataScript.findObjectFromParent("L", transform).transform.rotation = Quaternion.Euler(0, 0, -30);
        DataScript.findObjectFromParent("shadow", Camera.main.transform).SetActive(false);

        GetComponent<Animator>().SetInteger("state", 1);  //first time surprice
        yield return new WaitForSeconds(0.05f);
        GetComponent<Animator>().SetInteger("state", 0);

        yield return new WaitForSeconds(2);

        cam.GetComponent<cameraScript>().tempShakeTime = 1.5f;
        cam.GetComponent<cameraScript>().shakeCamera = true;

        yield return new WaitForSeconds(2f);

        GameObject CGani = DataScript.findObjectFromParent("Animation1", GameObject.Find("CGAnimation").transform);
        CGani.SetActive(true);
        CGani.transform.GetChild(0).GetComponent<Animator>().SetInteger("state", 1); 

        yield return new WaitForSeconds(3.2f);
        CGani.SetActive(false);


        int ShiftTime = 2 * 100;  
        float xTarget = 8;
        float yTarget = 6.4f;
        float xShift = (xTarget - cam.transform.position.x) / (float)ShiftTime;
        float yShift = (yTarget - cam.transform.position.y) / (float)ShiftTime;

        while (ShiftTime > 0)  //shift camera
        {
            cam.transform.position = new Vector3(cam.transform.position.x + xShift, cam.transform.position.y + yShift, -10);
            ShiftTime -= 1;
            yield return new WaitForSeconds(0.015f);
        }

        monster.SetActive(true);  
        monster.GetComponent<Animator>().SetInteger("state", 1);

        yield return new WaitForSeconds(monster.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length * monster.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed);
        GetComponent<Animator>().SetInteger("state", 1);  //second time surprice
        yield return new WaitForSeconds(0.05f);
        GetComponent<Animator>().SetInteger("state", 0);

        yield return new WaitForSeconds(1.5f);

        DataScript.findObjectFromParent("shadow", Camera.main.transform).SetActive(true);
        DataScript.currentCharacter = this.gameObject;
        DataScript.findFromAllObject("UI").SetActive(true);

    }


    public IEnumerator monsterChase()
    {

        GameObject monster = DataScript.findFromAllObject("monsterForAni");
        monster.SetActive(true);
        monster.GetComponent<Animator>().enabled = false;
        monster.GetComponent<SkeletonGraphic>().color = Color.white;

        SkeletonAnimation skeAni = GetComponent<SkeletonAnimation>();
        monster.GetComponent<BoxCollider2D>().enabled = false;

        GameObject keyLogo = DataScript.findObjectFromParent("keyLogo", GameObject.Find("specialEffect").transform);
        keyLogo.SetActive(true);
        keyLogo.GetComponent<keyLogo>().keepOpening = true;
        keyLogo.GetComponent<keyLogo>().keyCode = "Shift";
        DataScript.characterInteractable = false;

        while (transform.position.x >= -5)
        {
            if (skeAni.AnimationName == "idle" || skeAni.AnimationName == "idleR" || skeAni.AnimationName == "idleL")
            {
                if (transform.position.x < 5.8) monster.GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "onlyhiar_L",true);
                else if (transform.position.x > 10) monster.GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "onlyhiar_R", true);
                else monster.GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "onlyhiar_fronthead", true);
            }
            else if (skeAni.AnimationName == "runL" || skeAni.AnimationName == "runR" || skeAni.AnimationName == "backrun" || skeAni.AnimationName == "Front")
            {
                StartCoroutine(monster.GetComponent<monster>().chasing());
                keyLogo.GetComponent<keyLogo>().keepOpening = false;
                keyLogo.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(0.01f);
        }

        keyLogo.GetComponent<keyLogo>().keepOpening = false;
        keyLogo.SetActive(false);
        StartCoroutine(monsterChasedCheckPoint());
    }



    public IEnumerator monsterChasedCheckPoint()
    {
        yield return new WaitUntil(() => (transform.position.x >= -14 && transform.position.y <= -9.4));
        DataScript.characterInteractable = true;
        Animator mons = GameObject.Find("monsterForAni").GetComponent<Animator>();
        mons.enabled = true;
        if (mons.GetInteger("state") == 1)
        {
            DataScript.nextStoryBranch = "notBeingChased";
        }
        else if (mons.GetInteger("state") == 2)
        {
            DataScript.nextStoryBranch = "beingChased";
        }
    }

    public IEnumerator monsterChaseFromStairs() {
        GetComponent<Animator>().SetInteger("state", 1);
        yield return new WaitUntil(() => (transform.position.x > -16.7 && transform.position.y < -9));
        spineComponent.AnimationName = "runR";
        
    }

    public IEnumerator lookBack()
    {
        spineComponent.AnimationName = "LPair";
        spineComponent.timeScale = 0;
        DataScript.findObjectFromParent("L", transform).SetActive(true);
        DataScript.findObjectFromParent("L", transform).transform.rotation = Quaternion.Euler(0, 0, -30);
        DataScript.findObjectFromParent("shadow", Camera.main.transform).SetActive(false);
        yield return null;
    }


}
