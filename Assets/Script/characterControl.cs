using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;
using System;

public class characterControl : MonoBehaviour
{

    public Rigidbody2D rb; //自己的rigidbody組件
    public float originSpeed,originGravity; //初始速度
    public float speed; //當前速度
    public SkeletonAnimation spineComponent; //自己的動畫組件
    public bool needLowerLayer ; //判斷是否需要調整角色渲染層（物件遮擋）
    int originalSortOrder;

    public GameObject lightBarUI,flashLightR,flashLightL,shadow;
    Vector2 mouse,handPosition;
    float angle;

    void Start()
    {
        shadow = Camera.main.gameObject.transform.GetChild(0).gameObject;
        lightBarUI = DataScript.findObjectFromParent("lightBarBottom", DataScript.findFromAllObject("UI").transform);
        originalSortOrder = GetComponent<MeshRenderer>().sortingOrder;
        originGravity = 1;
        needLowerLayer = false;
        originSpeed = 5;
        rb = this.GetComponent<Rigidbody2D>();
        spineComponent = this.GetComponent<SkeletonAnimation>();
    }

    void Update()
    {
        if (DataScript.currentCharacter==this.gameObject)
        {
            if (!DataScript.waitForAnyKey)
            {
                mouse = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
                handPosition = new Vector2(transform.position.x, transform.position.y + 1.3f);
                angle = 70 * Mathf.Atan((float)(mouse.y - handPosition.y) / (mouse.x - handPosition.x));
                flashLight();
                movement();
                DirectionControl();
                AnimationControl();
            }
        }
        else if (Input.GetAxis("Horizontal") == 0 && !DataScript.ObjectsBeingControl.Contains(this.gameObject))
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        if (needLowerLayer) //控制物體遮擋
            GetComponent<MeshRenderer>().sortingOrder = 4;
        else
            GetComponent<MeshRenderer>().sortingOrder = originalSortOrder;

    }

    public void movement() //玩家控制移動
    {

        float horizontalMove, verticalMove;
        horizontalMove = DataScript.horizontalControlEnable ? Input.GetAxis("Horizontal") : 0; //如果該軸不可控制，其輸入永遠為0
        verticalMove = DataScript.verticalControlEnable ? Input.GetAxis("Vertical") : 0; 
        speed = originSpeed; //每幀初始化速度

        if (Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftAlt) && DataScript.strength > 0) //推動物體狀態
        {
            speed /= 4;
            if(DataScript.canCollide) this.gameObject.layer = 6; //可碰撞物體狀態
        }
        else this.gameObject.layer = 3; //3為玩家層，6為碰撞物體層
        if (DataScript.horizontalControlEnable) 
        {
            if (Input.GetKey(KeyCode.LeftShift) && !(Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftAlt)) && DataScript.strength > 0)
            {
                speed /= 2.5f;  //蹲下狀態，蹲下時不可移動物體，速度減慢
            }  
            if (horizontalMove == 0) rb.velocity = new Vector2(0, rb.velocity.y); //如果沒輸入，馬上停下
            else if (Input.GetKey(KeyCode.Space)) //開電筒狀態
            {
                if (((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && DataScript.characterDirection == "L" && horizontalMove < 0) || ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && DataScript.characterDirection == "R" && horizontalMove > 0))
                    rb.velocity = new Vector2(horizontalMove * speed, rb.velocity.y);
                else rb.velocity = new Vector2(0, rb.velocity.y); //如果沒輸入，馬上停下
            }
            else if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))) //普通行走狀態
                rb.velocity = new Vector2(horizontalMove * speed, rb.velocity.y);
            else rb.velocity = new Vector2(0,rb.velocity.y); //如果沒輸入，馬上停下
        }
        if (DataScript.verticalControlEnable)
        {
            GetComponent<Rigidbody2D>().gravityScale = 0; //爬樓梯時，重力設置為0
            if (Input.GetKey(KeyCode.LeftShift)) rb.velocity = new Vector2(rb.velocity.x, 0); //蹲下的時候，不可上下行動
            else if (verticalMove == 0) rb.velocity = new Vector2(rb.velocity.x, 0);//如果沒輸入，馬上停下
            else if (((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && DataScript.characterDirection == "B" && verticalMove > 0) || ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && DataScript.characterDirection == "F" && verticalMove < 0))
            {
                speed /= 1.5f;
                rb.velocity = new Vector2(rb.velocity.x, verticalMove * speed);
            }
            else rb.velocity = new Vector2(rb.velocity.x, 0);

        }
        else
        {
            GetComponent<Rigidbody2D>().gravityScale = originGravity;//重置重力
        } 

    }

    public void DirectionControl() //反映角色方向
    {
        if (!Input.GetKey(KeyCode.Space))
        {
            if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && !(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))) DataScript.characterDirection = "L";
            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && !(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))) DataScript.characterDirection = "R";
            if (!((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))))
            {
                if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))) DataScript.characterDirection = "B";
                else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) DataScript.characterDirection = "F";
            }
        }
        
    }

    public void AnimationControl() //控制角色動畫
    {
        spineComponent.timeScale = 1;
        if (Input.GetKey(KeyCode.LeftShift) && DataScript.strength > 0) //移動物體不可以蹲下
        {
            if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))&& DataScript.characterDirection=="L" ) spineComponent.AnimationName = "KneelrunL";
            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && DataScript.characterDirection == "R") spineComponent.AnimationName = "KneelrunR";
            else spineComponent.AnimationName= (DataScript.characterDirection == "L" ? "KneelL" : "KneelR");
        }
        else if ((Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftAlt)) && DataScript.strength > 0) //推動物體
        {
            spineComponent.timeScale = 0; //因為沒有靜止推動的動作，所以把動畫暫停拿來代替
            spineComponent.AnimationName = (DataScript.characterDirection == "L" ? "L push" : "R push");
            
            if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && DataScript.characterDirection == "L") //向左推
            {
                spineComponent.AnimationName = "L push";
                spineComponent.timeScale = 1;
            }
            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && DataScript.characterDirection == "R")//向右推
            {
                spineComponent.AnimationName = "R push";
                spineComponent.timeScale = 1;
            }
        }
        else //普通行走
        {
            //走動
            if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && DataScript.characterDirection == "L" && rb.velocity.x < 0)
            {
                if (Input.GetKey(KeyCode.Space) && DataScript.lightElectric != null) spineComponent.AnimationName = "LPair";
                else spineComponent.AnimationName = "runL";
            }

            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && DataScript.characterDirection == "R" && rb.velocity.x > 0) {
                if (Input.GetKey(KeyCode.Space) && DataScript.lightElectric != null) spineComponent.AnimationName = "RPair";
                else spineComponent.AnimationName = "runR";
            }
            else if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && DataScript.characterDirection == "B" && rb.velocity.x == 0) spineComponent.AnimationName = "backrun";
            else if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && DataScript.characterDirection == "F" && rb.velocity.x == 0) spineComponent.AnimationName = "Front";
            else if (Input.GetKey(KeyCode.Space) && DataScript.lightElectric != null) {
                switch (DataScript.characterDirection)
                {
                    case "L":
                        spineComponent.AnimationName = "LPair";
                        spineComponent.timeScale = 0;
                        break;
                    case "R":
                        spineComponent.AnimationName = "RPair";
                        spineComponent.timeScale = 0;
                        break;
                }

            }
            else switch (DataScript.characterDirection) //站立
                {
                    case "L":
                        spineComponent.AnimationName = "idleL";
                        break;
                    case "R":
                        spineComponent.AnimationName = "idleR";
                        break;
                    case "B":
                        if (rb.velocity.x == 0) spineComponent.AnimationName = "back";
                        break;
                    case "F":
                        if (rb.velocity.x == 0) spineComponent.AnimationName = "idle";
                        break;
                }
        }
    }

    private void OnTriggerStay2D(Collider2D collider) 
    {
        if (DataScript.currentCharacter == this.gameObject) //只在玩家控制的時候檢測
        {
            if (collider.gameObject.tag == "climbingArea") //爬梯區域
            {
                DataScript.verticalControlEnable = collider.gameObject.GetComponent<climbingAreaScript>().checkCharacterInside(); //如果角色在爬梯區域裡面，就開放y軸控制
                collider.gameObject.GetComponent<climbingAreaScript>().setUnableToCollide(); //爬樓梯時不可碰撞物體（防止與推梯子代碼衝突）
            }
        }
        if (collider.gameObject.tag == "needLowerLayer") //需要切換渲染層
        {
            needLowerLayer = true;
        }
        
    }
    private void OnTriggerExit2D(Collider2D collider) 
    {
        if (DataScript.currentCharacter == this.gameObject)//只在玩家控制的時候檢測
        {
            if (collider.gameObject.tag == "climbingArea")//爬梯區域
            {
                DataScript.verticalControlEnable = false;
                DataScript.canCollide = true; //可碰撞
            }
        }
        if (collider.gameObject.tag == "needLowerLayer")//需要切換渲染層
        {
            needLowerLayer = false;
        }
        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "floor")
        {
                originGravity= 10;
        }
        
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "floor")
        {
                originGravity = 0;
        }
        
    }

    public void flashLight()
    {
        if ((DataScript.lightElectric == null || DataScript.lightElectric<=0))
        {
            lightBarUI.SetActive(false);
        }
        else
        {
            lightBarUI.SetActive(true);

            if (Input.GetKey(KeyCode.Space) && !(Input.GetKey(KeyCode.LeftShift)) && !(Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.LeftAlt)))   // 蹲下與推東西的時候不可開電筒 
            {
                if (mouse.x < handPosition.x) DataScript.characterDirection = "L";
                else DataScript.characterDirection = "R";

                lightBarUI.GetComponent<CanvasGroup>().alpha = 1; //電筒UI可見
                GameObject.Find("lightBar").GetComponent<Image>().fillAmount = DataScript.lightElectric ?? 1; //傳入電筒電力
                DataScript.lightElectric -= (Time.deltaTime * 0.03f); //每幀減少電力

                shadow.SetActive(false);
                if (DataScript.characterDirection == "R") //角色向右，電筒向右
                {
                    flashLightR.SetActive(true);
                    if (DataScript.lightElectric <= 0) flashLightR.transform.GetChild(0).gameObject.SetActive(false);
                    else flashLightR.transform.GetChild(0).gameObject.SetActive(true);
                    if (needLowerLayer) flashLightR.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;
                    else flashLightR.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sortingOrder = originalSortOrder + 1;

                    flashLightL.SetActive(false);
                    flashLightR.transform.rotation = Quaternion.Euler(0, 0, angle+180);

                }
                else if (DataScript.characterDirection == "L") //角色向左，電筒向左
                {
                    flashLightR.SetActive(false);

                    flashLightL.SetActive(true);
                    if (DataScript.lightElectric <= 0) flashLightL.transform.GetChild(0).gameObject.SetActive(false);
                    else flashLightL.transform.GetChild(0).gameObject.SetActive(true);
                    if (needLowerLayer) flashLightL.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;
                    else flashLightL.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sortingOrder = originalSortOrder+1;

                    flashLightL.transform.rotation = Quaternion.Euler(0, 0, angle);
                }
            }
            else //電筒關閉狀態
            {
                lightBarUI.GetComponent<CanvasGroup>().alpha = 0.15f;
                flashLightR.SetActive(false);
                flashLightL.SetActive(false);
                shadow.SetActive(true);
            }
        }
    }

}
