using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class saveBox : MonoBehaviour
{
    public bool canOpen=false;
    public GameObject saveBoxPic;
    public GameObject flashLightPic;
    GameObject originCharacter;
    public GameObject keyLogo;
    bool openned=false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DataScript.currentCharacter == null) transform.GetChild(0).gameObject.SetActive(false);
        if (canOpen)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                openned = true;
                keyLogo.SetActive(false);
                originCharacter = DataScript.currentCharacter;
                DataScript.currentCharacter = null ;
                saveBoxPic.GetComponent<CanvasGroup>().alpha = 1;
                saveBoxPic.GetComponent<CanvasGroup>().blocksRaycasts = true;
                saveBoxPic.GetComponent<CanvasGroup>().interactable = true;            
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter && DataScript.characterInteractable)
        {
            this.transform.GetChild(0).gameObject.SetActive(true);
            if (!openned) keyLogo.SetActive(true);
            canOpen = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter && DataScript.characterInteractable)
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
            keyLogo.SetActive(false);
            canOpen = false;
        }
    }

    public void flashLightGet()
    {
        flashLightPic.SetActive(false);
        DataScript.lightElectric = 1;
        DataScript.backpack.Add(1);
        missionObjDescrip.id = 1;
        missionObjDescrip.show();
    }
    public void ExitSaveBox()
    {
        openned = false;
        saveBoxPic.GetComponent<CanvasGroup>().alpha = 0;
        saveBoxPic.GetComponent<CanvasGroup>().blocksRaycasts = false;
        saveBoxPic.GetComponent<CanvasGroup>().interactable =false;
        DataScript.currentCharacter = originCharacter;
    }


}
