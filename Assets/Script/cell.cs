using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class cell : MonoBehaviour
{
    public Image itemImage;

    Sprite sprite;
    int id;
    int listOrder;

    void Start()
    {
        listOrder = Int32.Parse(this.gameObject.name);
        id = DataScript.backpack[listOrder];
        sprite = gameData.missionObjs[id].sprite;

        itemImage.sprite = sprite;
    }

    void Update()
    {
        if (id != DataScript.backpack[listOrder])
            UpdateData();
    }

    void UpdateData()
    {
        id = DataScript.backpack[listOrder];
        sprite = gameData.missionObjs[id].sprite;

        itemImage.sprite = sprite;
    }

    public void openInfoPage()
    {
        missionObjDescrip.id = id;
        missionObjDescrip.show();
    }

}
