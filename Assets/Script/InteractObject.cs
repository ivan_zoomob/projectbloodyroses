using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractObject : MonoBehaviour
{
    public bool canOpen = false;
    public GameObject miniGame;
    public Material outlineMaterial;
    public int count=0;
    bool notOpened=true;
    public GameObject drawer;
    public GameObject drawerButton;
    public bool Book=false;
    public bool photo=false;
    public bool branchFinished = false;
    public GameObject exitButtonOfThedrawer;
    public Image timeBar;
    public bool timeCountingStart=false;
    public float timeRemaining=1;
    public GameObject lockerPlane;
    public List<GameObject> pieces;
    public GameObject keyLogo;
    bool openned=false;
    GameObject originChar = null;

    public static bool waitForMeetMonster=false;

    // Start is called before the first frame update
    void Start()
    {
        pieces = new List<GameObject>();
        for (int i = 1; i <= 6; i++) pieces.Add(lockerPlane.transform.GetChild(i).gameObject);

    }

    // Update is called once per frame
    void Update()
    {
        if (DataScript.currentCharacter == null) transform.GetChild(0).gameObject.SetActive(false);

        if (timeBar!=null && DataScript.backpack.Contains(8)) timeBar.fillAmount = timeRemaining;
        if (timeCountingStart && timeRemaining>0 && DataScript.backpack.Contains(8))
        {
            timeRemaining -= Time.deltaTime * 0.06f;
        }
        if (timeRemaining <= 0 && !branchFinished && DataScript.backpack.Contains(8))
        {
            timeCountingStart = false;
            foreach (GameObject piece in pieces) piece.GetComponent<movePieceInMinigame>().GameOver();
            DataScript.nextStoryBranch = "timesUp";
            branchFinished = true;
        }

        if (canOpen)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                openned = true;
                keyLogo.SetActive(false);
                drawer.SetActive(true);
                originChar = DataScript.currentCharacter;
                DataScript.currentCharacter = null;
                drawerButton.layer = 2;
            }
        }
        if (count == 6 && notOpened)
        {
            timeCountingStart = false;
            StartCoroutine(playLockerAnimation());
            drawerButton.layer = 7;
            DataScript.playedMiniGame = true;
        }
        if (Book && photo && !branchFinished)
        {
            exitButtonOfThedrawer.SetActive(true);
            branchFinished = true;
        }
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter && DataScript.characterInteractable)
        {
            if (!openned) keyLogo.SetActive(true);
            this.transform.GetChild(0).gameObject.SetActive(true);
            canOpen = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter && DataScript.characterInteractable)
        {
            keyLogo.SetActive(false);
            this.transform.GetChild(0).gameObject.SetActive(false);
            canOpen = false;
        }
    }

    public void openMiniGame()
    {
        miniGame.SetActive(true);
        Camera.main.eventMask = 1 << 7;
        exitButtonOfThedrawer.SetActive(false);
        if (DataScript.backpack.Contains(8)) DataScript.nextStoryBranch = "playingPuzzle";
        else
        {
            DataScript.nextStoryBranch = "playingPuzzleWithoutTiming";
            timeBar.enabled = false;
        }
        StartCoroutine(checkDialogueEnd());
    }

    public IEnumerator checkDialogueEnd()
    {
        yield return new WaitUntil(() => (GameObject.Find("Dialogue") != null));
        yield return new WaitUntil(() => (GameObject.Find("Dialogue") == null));
        if (DataScript.backpack.Contains(8)) timeCountingStart = true;
    }

    public void ExitBox()
    {
        miniGame.SetActive(false);
    }

    public void ExitThisPage(GameObject itself)
    {
        openned = false;
        itself.transform.parent.gameObject.SetActive(false);
        DataScript.currentCharacter = originChar;
        Camera.main.eventMask = ~0;

        if (Book && photo && branchFinished)
        {
            DataScript.nextStoryBranch = "meetMonster";
            waitForMeetMonster = true;
        }
    }

    IEnumerator playLockerAnimation()
    {
        notOpened = false;
        Animator ani = miniGame.GetComponentsInChildren<Animator>()[0];
        ani.enabled = true;
        yield return new WaitUntil(() => (ani.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1));
        ani.enabled = false;
        yield return new WaitForSeconds(0.5f);

        CanvasGroup miniGameCanvasGp = miniGame.GetComponent<CanvasGroup>();
        while (miniGameCanvasGp.alpha > 0)
        {
            miniGameCanvasGp.alpha -= 0.05f;
            yield return new WaitForSeconds(0.05f);
        }
        yield return new WaitForSeconds(0.3f);
        Destroy(miniGame);
        GameObject smallLocker = drawer.transform.GetChild(drawer.transform.childCount - 1).gameObject;
        smallLocker.GetComponent<Button>().enabled = false;
        CanvasGroup lockerCanvasGp = smallLocker.GetComponent<CanvasGroup>();
        while (lockerCanvasGp.alpha>0)
        {
            lockerCanvasGp.alpha -= 0.05f;
            yield return new WaitForSeconds(0.05f);
        }
        Destroy(smallLocker.GetComponent<lockerButton>().clone);
        Destroy(smallLocker);

        yield return new WaitForSeconds(0.3f);
        drawerButton.GetComponent<drawerContent>().enabled = true;
        drawerButton.GetComponent<Button>().enabled = true;
        
    }

    public void takeBook(GameObject self)
    {
        Book = true;
        self.SetActive(false);
        DataScript.backpack.Add(3);
        missionObjDescrip.id = 3;
        missionObjDescrip.show();
    }
    public void takePhoto(GameObject self)
    {
        photo = true;
        self.SetActive(false);
        DataScript.backpack.Add(2);
        missionObjDescrip.id = 2;
        missionObjDescrip.show();
    }

}

