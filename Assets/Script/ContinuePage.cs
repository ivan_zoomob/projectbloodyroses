using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class ContinuePage : MonoBehaviour
{
    [SerializeField]
    Canvas canvasContinuePage;

    public void ButtonClose()
    {
        EnableContinuePage(false);
    }

    public void EnableContinuePage(bool stat)
    {
        canvasContinuePage.enabled = stat;
    }

    public void LoadData(GameObject dataObject)
    {
        int DataNumber;
        if (dataObject.name.Length < 10) DataNumber = 0;
        else DataNumber = Int32.Parse(dataObject.name.ToCharArray()[dataObject.name.Length - 2].ToString());
        int sceneNum=PlayerPrefs.GetInt(DataNumber + "_"+"Scene");
        SceneManager.LoadScene(sceneNum);
        DataScript.dataNumber = DataNumber;

    }
}
