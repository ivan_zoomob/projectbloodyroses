using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortLayerChanger : MonoBehaviour
{
    [Header("please set to player")]
    [SerializeField] LayerMask detectLayer;
    [Header("sort layer")]
    [SerializeField] int sortLayerValue;
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))   //if enter trigger into object with detectLayer
        {
            collision.gameObject.GetComponentInParent<CharacterAnimationPlayer>().ChangeSortLayer(sortLayerValue);
        }
    }
}
