using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedChanger : MonoBehaviour
{
    [Header("please set to player")]
    [SerializeField] LayerMask detectLayer;
    [Header("speed value")]
    [SerializeField] float speedBoost;
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))   //if enter trigger into object with detectLayer
        {
            collision.gameObject.GetComponentInParent<CharacterController>().ChangeSpeedBoost(speedBoost);
        }
    }
}
