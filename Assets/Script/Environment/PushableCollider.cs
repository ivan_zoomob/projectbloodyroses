using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableCollider : MonoBehaviour
{
    [Header("please set to player")]
    [SerializeField] LayerMask detectLayer;
    [SerializeField] GameObject baseObject;         //object to be push
    [SerializeField] Transform objectEndPointLeft;
    [SerializeField] Transform objectEndPointRight;

    CharacterAnimationPlayer charAnimationPlayer;   //pusher

    bool isLeft;                            //true if this is the left collider, else right
    bool isTrigger = false;                 //if player enters collider
    bool isStartingPosAssigned = false;     //if distant between pusher and object is calculated

    Vector3 pushingDistance;                //starting pos of pusher and object

    private void Start()
    {
        isLeft = (transform.position.x < baseObject.transform.position.x);  //determine if collider is left or right
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))   //if pusher enters collider
        {
            isTrigger = true;
            charAnimationPlayer = collision.GetComponentInParent<CharacterAnimationPlayer>();   //get pusher info
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {

        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))   //if pusher exit collider
        {
            isTrigger = false;
            charAnimationPlayer = null;
            isStartingPosAssigned = false;
        }
    }

    private void Update()
    {
        if(charAnimationPlayer != null && isTrigger)    
        {
            if(charAnimationPlayer.characterPose == CharacterAnimationPlayer.Pose.push)
            {
                if(isLeft && charAnimationPlayer.characterPoseDirection == CharacterAnimationPlayer.PoseDirection.right)    //if pusher is present + doing correct direction push
                {
                    //if isLeft & player pushing right

                    //calculate distance between pusher and object
                    AssignStartingDistance(charAnimationPlayer.transform.position);
                    //move object using that distance
                    if(objectEndPointRight.position.x > baseObject.transform.position.x)
                    {
                        baseObject.transform.position = new Vector3((charAnimationPlayer.transform.position.x + pushingDistance.x), baseObject.transform.position.y, baseObject.transform.position.z);  
                    }
                }
                else if (!isLeft && charAnimationPlayer.characterPoseDirection == CharacterAnimationPlayer.PoseDirection.left)
                {
                    //if is Right & player pushing left

                    //same
                    AssignStartingDistance(charAnimationPlayer.transform.position);
                    if(objectEndPointLeft.position.x < baseObject.transform.position.x)
                    { 
                        baseObject.transform.position = new Vector3((charAnimationPlayer.transform.position.x + pushingDistance.x), baseObject.transform.position.y, baseObject.transform.position.z);
                    }
                }
                else
                {
                    isStartingPosAssigned = false;  //reset distance between object and pusher
                }
            }
            else
            {
                isStartingPosAssigned = false;
            }
        }
    }

    void AssignStartingDistance(Vector3 pos)    //calculate the distance between object and pusher
    {
        if(isStartingPosAssigned == false)
        {
            pushingDistance = baseObject.transform.position - pos;
            isStartingPosAssigned = true;
        }
    }
}
