using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slope : MonoBehaviour
{
    [Header("Set to player please")]
    [SerializeField] LayerMask detectLayer;

    [Header("left , right position")]
    [SerializeField] Transform pointA;
    [SerializeField] Transform pointB;

    [Header("Speed multiplier")]
    [SerializeField] float speedBoost;

    [SerializeField] bool inTrigger = false;

    float posPercentage = 0f;
    Vector2 finalSlopePosition;

    CharacterController charController;

    void Start()
    {
        charController = FindObjectOfType<CharacterController>();
    }

    void Update()
    {
        //Debug.Log(inTrigger);
        if(inTrigger)
        {
            if(charController.horiInput != 0)
            {
                if(charController.horiInput < 0)
                {
                    //left
                    posPercentage = posPercentage - speedBoost * Time.deltaTime;
                    posPercentage = Mathf.Clamp01(posPercentage);
                }
                else
                {
                    //right
                    posPercentage = posPercentage + speedBoost * Time.deltaTime;
                    posPercentage = Mathf.Clamp01(posPercentage);
                }
            }
            if(posPercentage > 0 && posPercentage < 1)
            {
                finalSlopePosition = Vector2.Lerp(pointA.position, pointB.position, posPercentage);
                charController.MoveCharacterByPosition(finalSlopePosition);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))    //if exit trigger from object with detectLayer
        {
            if (Vector2.Distance(collision.gameObject.transform.position,pointA.position) < Vector2.Distance(collision.gameObject.transform.position, pointB.position))
            {
                //start from left
                //posPercentage = 0f;
            }
            else
            {
                //start from right
                //posPercentage = 1f;
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))   //if enter trigger into object with detectLayer
        {
            inTrigger = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))    //if exit trigger from object with detectLayer
        {
            inTrigger = false;
        }
    }
}
