using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxAreaScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
        transform.localPosition = new Vector3(20, -4, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (transform.childCount <= 8) GetComponent<RectTransform>().sizeDelta = new Vector2(1830f, 865f);
        else GetComponent<RectTransform>().sizeDelta = new Vector2(434 * Mathf.Ceil(transform.childCount/2 + 0.2f),865f);
    } 
}
