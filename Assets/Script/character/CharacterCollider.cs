using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCollider : MonoBehaviour
{
    enum colliderPos {up,down,left,right};

    [SerializeField] LayerMask detectLayer;     
    [SerializeField] colliderPos colliderLocation;
    CharacterController charController;
    private void Awake()
    {
        charController = GetComponentInParent<CharacterController>();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (detectLayer == (detectLayer | (1 << collision.gameObject.layer)))   //if enter trigger into object with detectLayer
        {
            charController.CharacterColliderTrigger((int)colliderLocation, true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(detectLayer == (detectLayer | (1 << collision.gameObject.layer)))    //if exit trigger from object with detectLayer
        {
            charController.CharacterColliderTrigger((int)colliderLocation, false);
        }
    }
}
