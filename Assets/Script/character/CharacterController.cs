using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [Header("Assign please")]
    [SerializeField] Transform character;
    public float moveSpeed = 5f;

    [SerializeField]bool[] allowDirectionInput = new bool[4];   //allow direction input ( 0:up   1:down  2:left  3:right )

    [HideInInspector]
    public bool allowUserInput = true;          //control by other module

    CharacterAnimationPlayer charAniPlayer;

    //user input
    [HideInInspector]
    public float vertInput = 0f;
    [HideInInspector]
    public float horiInput = 0f;

    float speedBoost = 1f;
    //result movement
    Vector3 movingDirection = new Vector3(0f,0f,0f);

    void Awake()
    {
        charAniPlayer = gameObject.GetComponentInChildren<CharacterAnimationPlayer>();
    }

    void Update()
    {
        if(allowUserInput)
        {
            UserInput();
        }
    }


    void UserInput()    //get user input and change animation player state
    {
        vertInput = Input.GetAxis("Vertical");
        horiInput = Input.GetAxis("Horizontal");

        //pose directions
        if (vertInput != 0)
        {
            if (vertInput > 0f)
            {
                //up
                charAniPlayer.characterPoseDirection = CharacterAnimationPlayer.PoseDirection.back;
                if(allowDirectionInput[0])
                {
                    movingDirection.y = vertInput;
                }
                else
                {
                    movingDirection.y = 0;
                }
            }
            else
            {
                //down
                charAniPlayer.characterPoseDirection = CharacterAnimationPlayer.PoseDirection.front;
                if (allowDirectionInput[1])
                {
                    movingDirection.y = vertInput;
                }
                else
                {
                    movingDirection.y = 0;
                }
            }
        }
        else
        {
            movingDirection.y = 0;
        }

        if (horiInput != 0)
        {
            if (horiInput < 0f)
            {
                //left
                charAniPlayer.characterPoseDirection = CharacterAnimationPlayer.PoseDirection.left;
                if (allowDirectionInput[2])
                {
                    movingDirection.x = horiInput;
                }
                else
                {
                    movingDirection.x = 0;
                }
            }
            else
            {
                //right
                charAniPlayer.characterPoseDirection = CharacterAnimationPlayer.PoseDirection.right;
                if (allowDirectionInput[3])
                {
                    movingDirection.x = horiInput;
                }
                else
                {
                    movingDirection.x = 0;
                }
            }
        }
        else
        {
            movingDirection.x = 0;
        }

        //move character
        MoveCharacterByDirection(movingDirection * moveSpeed * speedBoost * Time.deltaTime);

        //pose motion
        if ((vertInput != 0) || (horiInput != 0))
        {
            charAniPlayer.characterPoseMotion = CharacterAnimationPlayer.PoseMotion.run;
        }
        else
        {
            charAniPlayer.characterPoseMotion = CharacterAnimationPlayer.PoseMotion.idle;
        }

        //pose
        if(Input.GetKey(KeyCode.Space))
        {
            //light
            charAniPlayer.characterPose = CharacterAnimationPlayer.Pose.pair;
        }
        else if(Input.GetKey(KeyCode.LeftShift))
        {
            //kneel
            charAniPlayer.characterPose = CharacterAnimationPlayer.Pose.kneel;
            ChangeSpeedBoost(0.5f);
        }
        else if(Input.GetKey(KeyCode.F))
        {
            //push
            charAniPlayer.characterPose = CharacterAnimationPlayer.Pose.push;
            ChangeSpeedBoost(0.5f);
        }
        else
        {
            //normal
            charAniPlayer.characterPose = CharacterAnimationPlayer.Pose.normal;
            ChangeSpeedBoost(1f);
        }
    }

    public void MoveCharacterByDirection(Vector3 des)
    {
        character.Translate(des);
    }
    public void MoveCharacterByPosition(Vector3 des)
    {
        character.Translate(des - character.transform.position);
    }
    public void ChangeSpeedBoost(float value)
    {
        speedBoost = value;
    }
    public void CharacterColliderTrigger(int index, bool inbound)
    {
        allowDirectionInput[index] = inbound;
    }
}
