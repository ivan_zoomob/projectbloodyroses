using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class CharacterAnimationPlayer : MonoBehaviour
{
    [Header("Assign please")]
    [SerializeField] SkeletonAnimation characterAnimation;  //character spine

    public enum PoseMotion { idle, run };
    public enum Pose {normal,kneel,pair,push};
    public enum PoseDirection {left, right, front, back };

    [HideInInspector] public Pose characterPose = Pose.normal;
    [HideInInspector] public PoseMotion characterPoseMotion = PoseMotion.idle;
    [HideInInspector] public PoseDirection characterPoseDirection = PoseDirection.front;

    string currentAnimation = "empty";
    string prevAnimation = "null";

    MeshRenderer characterMeshRenderer; // mesh renderer of the character spine
    private void Start()
    {
        characterMeshRenderer = characterAnimation.GetComponent<MeshRenderer>();
    }

    void Update()
    {
        currentAnimation = characterPose.ToString() + " " + characterPoseMotion.ToString() + " " +characterPoseDirection.ToString();
        if (string.Equals(prevAnimation, currentAnimation) == false)
        {
            prevAnimation = currentAnimation;
            PlayCharacterAnimation();
        }
    }

    void PlayCharacterAnimation()   //play animation according to pose and pose direction
    {
        switch (currentAnimation) 
        {
            //normal run
            case "normal run front":
            case "normal run back":
            case "normal run left":
            case "normal run right":
                characterAnimation.AnimationName = currentAnimation;
                break;


            //normal idle
            case "normal idle front":
            case "normal idle back":
            case "normal idle left":
            case "normal idle right":
                characterAnimation.AnimationName = currentAnimation;
                break;


            //kneel run
            case "kneel run left":
            case "kneel run right":
                characterAnimation.AnimationName = currentAnimation;
                break;
            case "kneel run front":             //no such animation, dont update animation
            case "kneel run back":
                break;


            //kneel idle
            case "kneel idle left":          
            case "kneel idle right":
                characterAnimation.AnimationName = currentAnimation;
                break;
            case "kneel idle front":             //no such animation, kneel left / right
            case "kneel idle back":
                if (Random.Range(0f, 1f) > 0.5f)
                {
                    characterPoseDirection = PoseDirection.left;
                }
                else
                {
                    characterPoseDirection = PoseDirection.right;
                }
                characterAnimation.AnimationName = currentAnimation;
                break;


            //push run
            case "push run left":
            case "push run right":
                characterAnimation.AnimationName = currentAnimation;
                break;
            case "push run front":             //no such animation
            case "push run back":
                break;


            //push idle
            case "push idle left":
            case "push idle right":
                characterAnimation.AnimationName = currentAnimation;
                break;
            case "push idle front":             //no such animation
            case "push idle back":
                if (Random.Range(0f, 1f) > 0.5f)
                {
                    characterPoseDirection = PoseDirection.left;
                }
                else
                {
                    characterPoseDirection = PoseDirection.right;
                }
                characterAnimation.AnimationName = currentAnimation;
                break;


            //pair run
            case "pair run left":
            case "pair run right":
                characterAnimation.AnimationName = currentAnimation;
                break;
            case "pair run front":             //no such animation
            case "pair run back":
                break;


            //pair idle
            case "pair idle left":
            case "pair idle right":
                characterAnimation.AnimationName = currentAnimation;
                break;
            case "pair idle front":             //no such animation
            case "pair idle back":
                if (Random.Range(0f, 1f) > 0.5f)
                {
                    characterPoseDirection = PoseDirection.left;
                }
                else
                {
                    characterPoseDirection = PoseDirection.right;
                }
                characterAnimation.AnimationName = currentAnimation;
                break;
        }
    }

    public void ChangeSortLayer(int val)
    {
        characterMeshRenderer.sortingOrder = val;
    }
}
