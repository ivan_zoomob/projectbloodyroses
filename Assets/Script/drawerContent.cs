using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class drawerContent : MonoBehaviour
{
    public GameObject drawerOutline;
    bool openning = false;
    Animator Ani;

    Vector3 originPos;
    // Start is called before the first frame update
    void Start()
    {
        originPos = transform.position;
        Ani = transform.parent.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        drawerOutline.transform.position = transform.position;
    }

    private void OnMouseEnter()
    {
        drawerOutline.transform.position = transform.position;
        drawerOutline.SetActive(true);
    }

    private void OnMouseExit()
    {
        drawerOutline.SetActive(false);
    }

    public void drawerControl()
    {
        if (!openning)
        {
            Ani.SetInteger("changeState", 1);
            openning = true;
        }
        else
        {
            Ani.SetInteger("changeState", 2);
            openning = false;
        }
          
    }
}
