using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dialogue_name_box : MonoBehaviour
{
    public Text textbox;
    public CanvasGroup canvasLayer;
    // Start is called before the first frame update
    void Start()
    {
        textbox = this.GetComponentInChildren<Text>();
        canvasLayer = this.GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (textbox.text == "System") //系統旁白不需要角色名稱箱
            canvasLayer.alpha = 0;
        else canvasLayer.alpha = 1;
    }
}
