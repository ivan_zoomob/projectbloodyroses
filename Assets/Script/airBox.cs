using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class airBox : MonoBehaviour
{
    EdgeCollider2D col;
    // Start is called before the first frame update
    void Start()
    {
        col = this.GetComponent<EdgeCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (DataScript.isOnFloor) col.isTrigger = true;
        else col.isTrigger = false;
    }
    
}
