using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class missionObjDescrip : MonoBehaviour
{
    public static Text nameBox;
    public static Text describBox;
    public static Image logoBox;
    public static int id;

    public static CanvasGroup ownCanvasGp;
    public static Animator ownAnimator;
    private void Awake()
    {
        ownCanvasGp = GetComponent<CanvasGroup>();
        ownAnimator = GetComponent<Animator>();
        nameBox = DataScript.findObjectFromParent("Name", this.transform).GetComponent<Text>();
        describBox = DataScript.findObjectFromParent("Description", this.transform).GetComponent<Text>();
        logoBox = DataScript.findObjectFromParent("icon", this.transform).GetComponent<Image>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static void UpdateInfo(int id)
    {
        nameBox.text = gameData.missionObjs[id].name;
        describBox.text = gameData.missionObjs[id].description;
        logoBox.sprite = gameData.missionObjs[id].sprite;
    }
    public static void show()
    {
        UpdateInfo(id);
        ownAnimator.SetInteger("state", 1);
    }
    public static void close()
    {
        ownAnimator.SetInteger("state", 0);
    }

    public static void firstTimeClose()
    {
        ownAnimator.SetInteger("state", 2);
    }
}
