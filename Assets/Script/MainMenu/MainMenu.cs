using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    ContinuePage continuePage;
    private void Awake()
    {
        continuePage = FindObjectOfType<ContinuePage>();
    }

    public void ButtonNewGame()
    {
        SceneManager.LoadScene(1);
    }
    public void ButtonContinue()
    {
        continuePage.EnableContinuePage(true);
    }
    public void ButtonOptions()
    {
        //todo: enable option page
    }
    public void ButtonExit()
    {
        Application.Quit();
    }
}
