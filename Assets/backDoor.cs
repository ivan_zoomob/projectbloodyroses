using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backDoor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == DataScript.currentCharacter)
        {
            if (DataScript.backpack.Contains(8)) {
                if (DataScript.playedMiniGame) DataScript.nextStoryBranch = "DoYouWantOpenDoor";
                else DataScript.nextStoryBranch = "NotDoneMiniGame";
            }
            else DataScript.nextStoryBranch = "CantOpenDoor";
        }
    }
}
