using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class keyLogo : MonoBehaviour
{

    public Sprite FKey;
    public Sprite ShiftKey;
    public string keyCode;
    public bool keepOpening = false;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Image>().sprite = FKey;
    }

    // Update is called once per frame
    void Update()
    {

        if (keyCode == "F")
        {
            GetComponent<Image>().sprite = FKey;
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (keyCode == "Shift")
        {
            GetComponent<Image>().sprite = ShiftKey;
            transform.localScale = new Vector3(2, 1, 1);
        }

        if (DataScript.currentCharacter == null)
        {
            GetComponent<Image>().enabled = false;
        }
        else GetComponent<Image>().enabled = true;
    }

}
